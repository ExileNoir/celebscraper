package BookBoon.Boursorama;

import com.gargoylesoftware.htmlunit.HttpMethod;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.WebRequest;
import com.gargoylesoftware.htmlunit.util.NameValuePair;
import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static fr.boursorama.CelebScraperTwo.createWebClient;
import static fr.boursorama.CelebScraperTwo.setProxyConfig;

public class BookBoonScraperOne {

    private static final String BASE_URL = "https://bookboon.com/en/";
    private static final String BOOK_QUERY = "java-5-files-and-java-io-ebook";


    public static void main(String[] args) {

        WebClient client = createWebClient();
        setProxyConfig(client);

        try {

            WebRequest request = new WebRequest(new URL(BASE_URL + BOOK_QUERY), HttpMethod.POST);

            List<NameValuePair> params = new ArrayList<>();
            params.add(new NameValuePair("answers", "1400a987-7b18-4b5c-a63f-742cff3132cd"));
            params.add(new NameValuePair("answers", "82ca0cb6-81b1-e011-b817-22a08ed629e5"));
            params.add(new NameValuePair("answers", "96ce0cb6-81b1-e011-b817-22a08ed629e5"));
            params.add(new NameValuePair("consent", "false"));
            params.add(new NameValuePair("handle", "RB2P8RisxEwhL2RWhlftho8vH5kfcpR1"));

            request.setRequestParameters(params);

            File file = new File("Java5 Files and Java 10.pdf");
            OutputStream outputStream = new FileOutputStream(file);
            IOUtils.copy(client.getPage(request).getWebResponse().getContentAsStream(), outputStream);
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}


