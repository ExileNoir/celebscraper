package BookBoon.Boursorama;

import com.gargoylesoftware.htmlunit.HttpMethod;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.WebRequest;
import com.gargoylesoftware.htmlunit.util.NameValuePair;
import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static fr.boursorama.CelebScraperTwo.createWebClient;
import static fr.boursorama.CelebScraperTwo.setProxyConfig;

/**
 * A WebScraping Bot that downloads a automatically a certain PFD from site
 *
 * @author Steven Deseure
 * @see <a href="https://bookboon.com/en</a>" + add BOOK_QUERY
 * @see BookBoonScraperOne
 */
public class BookBoonScraperTwo {

    private static final String BASE_URL = "https://bookboon.com/en/";
    private static final String BOOK_QUERY = "java-5-files-and-java-io-ebook";
    private static final String BOOKBOON_ALL_QUESTIONS_PAGE = "https://bookboon.com/services/en/questions?answers=1400a987-7b18-4b5c-a63f-742cff3132cd&answers=80ca0cb6-81b1-e011-b817-22a08ed629e5&answers=f71fbc0c-6a1a-4401-8117-cd016a75915f";

    private static final String QUESTIONS = "questions";
    private static final String ID = "id";
    private static final String ANSWERS = "answers";
    private static final String CONSENT = "consent";
    private static final String HANDLE = "handle";

    private static final String CHECKBOX_FALSE = "//label/input[@value='false']";
    private static final String DOWNLOAD_BUTTON = "//button[@class='submit']";


    public static void main(String[] args) {

        WebClient client = createWebClient();
        setProxyConfig(client);

        try {
            JSONObject questionPageJson = new JSONObject(client.getPage(BOOKBOON_ALL_QUESTIONS_PAGE).getWebResponse().getContentAsString());

            JSONObject jsonObjectQuestionStudy = (JSONObject) questionPageJson.getJSONArray(QUESTIONS).get(0);
            JSONObject jsonObjectQuestionStudyProgram = (JSONObject) questionPageJson.getJSONArray(QUESTIONS).get(1);
            JSONObject jsonObjectQuestionUniversity = (JSONObject) questionPageJson.getJSONArray(QUESTIONS).get(2);

            List<String> idQuestionsStudy = getListIDs(jsonObjectQuestionStudy);
            List<String> idQuestionStudyProgram = getListIDs(jsonObjectQuestionStudyProgram);
            List<String> idQuestionUniversity = getListIDs(jsonObjectQuestionUniversity);

            List<NameValuePair> params = getNameValuePairs(idQuestionsStudy, idQuestionStudyProgram, idQuestionUniversity);

            WebRequest request = new WebRequest(new URL(BASE_URL + BOOK_QUERY), HttpMethod.POST);

            request.setRequestParameters(params);

            pfdCreator(client, request);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static List<String> getListIDs(JSONObject jsonObjectQuestionStudy) {
        JSONArray jsonArrayStudying = (JSONArray) jsonObjectQuestionStudy.get(ANSWERS);
        List<String> idQuestionsStudy = new ArrayList<>();
        jsonArrayExtracter(jsonArrayStudying, idQuestionsStudy);
        return idQuestionsStudy;
    }

    private static List<NameValuePair> getNameValuePairs(List<String> idQuestionsStudy, List<String> idQuestionStudyProgram, List<String> idQuestionUniversity) {
        List<NameValuePair> params = new ArrayList<>();
        params.add(new NameValuePair(ANSWERS, idQuestionsStudy.get(0)));
        params.add(new NameValuePair(ANSWERS, idQuestionStudyProgram.get(0)));
        params.add(new NameValuePair(ANSWERS, idQuestionUniversity.get(0)));
        params.add(new NameValuePair(CONSENT, CHECKBOX_FALSE));
        params.add(new NameValuePair(HANDLE, DOWNLOAD_BUTTON));
        return params;
    }

    private static void pfdCreator(WebClient client, WebRequest request) throws IOException {
        File file = new File("ENGLISH.pdf");
        OutputStream outputStream = new FileOutputStream(file);
        IOUtils.copy(client.getPage(request).getWebResponse().getContentAsStream(), outputStream);
    }

    private static void jsonArrayExtracter(JSONArray jsonArrayStudying, List<String> idQuestionsStudy) {
        for (int i = 0; i < jsonArrayStudying.length(); i++) {
            idQuestionsStudy.add((String) jsonArrayStudying.getJSONObject(i).get(ID));
        }
    }
}

