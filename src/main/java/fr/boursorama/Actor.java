package fr.boursorama;

/**
 * Creates an Actor class for Name && BirthDay
 *
 * @author Steven Deseure
 * @see CelebScraperOne
 * @see CelebScraperTwo
 * @see CelebScraperThree
 */
public class Actor {

    private String actorName;
    private String bDay;

    public Actor(String actorName, String bDay) {
        this.actorName = actorName;
        this.bDay = bDay;
    }

    public String getActorName() {
        return actorName;
    }

    public void setActorName(String actorName) {
        this.actorName = actorName;
    }

    public String getbDay() {
        return bDay;
    }

    public void setbDay(String bDay) {
        this.bDay = bDay;
    }

    public String toString() {
        return "Name: " + getActorName() + " Bday: " + getbDay();
    }

}
