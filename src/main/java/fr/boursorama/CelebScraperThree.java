package fr.boursorama;

import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlElement;
import com.gargoylesoftware.htmlunit.html.HtmlPage;

import java.util.ArrayList;
import java.util.List;

import static fr.boursorama.CelebScraperTwo.*;

/**
 * Scrapes the Full Website From A to Z with all Subpages for Actors names && BirthDays
 *
 * @author Steven Deseure
 * @see <a href="https://anniversaire-celebrite.com/abc-a.html</a>"
 * @see Actor
 * @see CelebScraperOne
 * @see CelebScraperTwo
 * @see CelebScraperTwo#createWebClient()
 * @see CelebScraperTwo#setProxyConfig(WebClient)
 * @see CelebScraperTwo#parsePageCelebs(WebClient, HtmlPage, String, String, String)
 */
public class CelebScraperThree {

    private static final String BASE_URL = "https://anniversaire-celebrite.com/";
    private static final String ALPHABET_URL = "abc-a.html";
    private static char alpha = 'a';

    private static final String GET_LAST_SUB_PAGE = "//ul[@class='pagination']/li[last()-1]/a";

    private static final String GET_ACTORS_PAGE = "//div[@class='column col-2 col-xl-2 col-lg-3 col-md-4 col-xs-6']/a";
    private static final String GET_ACTOR_BDAY = "//time[@itemprop='birthDate']";
    private static final String GET_ACTOR_NAME = "//h1[@class='h1celebrite']";

    private static List<Actor> actors = new ArrayList<Actor>();

    public static void main(String[] args) {

        WebClient client = createWebClient();

        setProxyConfig(client);

        try {
            HtmlPage mainPage = client.getPage(BASE_URL + ALPHABET_URL);

            for (int i = 26; i > 0; i--) {
                HtmlElement pageNumberHtmlElement = mainPage.getFirstByXPath(GET_LAST_SUB_PAGE);
                Integer pageNumber = Integer.parseInt((pageNumberHtmlElement).asText());
                String adjustedAlphabetUrl;


                for (int currentPage = 1; currentPage <= pageNumber; currentPage++) {
                    if (currentPage == 1) {
                        adjustedAlphabetUrl = ALPHABET_URL.substring(0, 4) + (alpha) + ALPHABET_URL.substring(5);
                        mainPage = client.getPage(BASE_URL + adjustedAlphabetUrl);

                        actors.addAll(parsePageCelebs(client, mainPage, GET_ACTORS_PAGE, GET_ACTOR_BDAY, GET_ACTOR_NAME));
                    } else {
                        adjustedAlphabetUrl = ALPHABET_URL.substring(0, 4) + (alpha) + "-" + (currentPage) + ALPHABET_URL.substring(5);
                        mainPage = client.getPage(BASE_URL + adjustedAlphabetUrl);

                        actors.addAll(parsePageCelebs(client, mainPage, GET_ACTORS_PAGE, GET_ACTOR_BDAY, GET_ACTOR_NAME));
                    }
                }
                ++alpha;

                for (Actor actor : actors) {
                    System.out.println(actor);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
