package fr.boursorama;

import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlPage;

import java.util.List;

import static fr.boursorama.CelebScraperTwo.*;

/**
 * Scrapes the Full Website for Actors names && BirthDays
 *
 * @author Steven Deseure
 * @see <a href="https://anniversaire-celebrite.com/abc-a.html</a>"
 * @see Actor
 * @see CelebScraperTwo
 * @see CelebScraperThree
 * @see CelebScraperTwo#createWebClient()
 * @see CelebScraperTwo#setProxyConfig(WebClient)
 * @see CelebScraperTwo#parsePageCelebs(WebClient, HtmlPage, String, String, String)
 */
public class CelebScraperOne {

    private static final String BASE_URL = "https://anniversaire-celebrite.com/";
    private static final String GET_ACTORS_PAGE = "//div[@class='column col-2 col-xl-2 col-lg-3 col-md-4 col-xs-6']/a";
    private static final String GET_ACTOR_BDAY = "//time[@itemprop='birthDate']";
    private static final String GET_ACTOR_NAME = "//h1[@class='h1celebrite']";

    public static void main(String[] args) {

        WebClient client = createWebClient();

        setProxyConfig(client);


        try {
            HtmlPage mainPage = client.getPage(BASE_URL);

            List<Actor> actors = parsePageCelebs(client, mainPage, GET_ACTORS_PAGE, GET_ACTOR_BDAY, GET_ACTOR_NAME);
            for (Actor a : actors) {
                System.out.println(a);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
