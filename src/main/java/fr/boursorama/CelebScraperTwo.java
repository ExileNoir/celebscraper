package fr.boursorama;

import com.gargoylesoftware.htmlunit.ProxyConfig;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlAnchor;
import com.gargoylesoftware.htmlunit.html.HtmlElement;
import com.gargoylesoftware.htmlunit.html.HtmlPage;

import java.util.ArrayList;
import java.util.List;

/**
 * Scrapes the Full Website From A to Z  for Actors names && BirthDays
 *
 * @author Steven Deseure
 * @see <a href="https://anniversaire-celebrite.com/abc-a.html</a>"
 * @see Actor
 * @see CelebScraperOne
 * @see CelebScraperThree
 * @see CelebScraperTwo#createWebClient()
 * @see CelebScraperTwo#setProxyConfig(WebClient)
 * @see CelebScraperTwo#parsePageCelebs(WebClient, HtmlPage, String, String, String)
 */
public class CelebScraperTwo {

    private static final String BASE_URL = "https://anniversaire-celebrite.com/";
    private static final String ALPHABET_URL = "abc-a.html";
    private static char alpha = 'a';

    private static final String GET_ACTORS_PAGE = "//div[@class='column col-2 col-xl-2 col-lg-3 col-md-4 col-xs-6']/a";
    private static final String GET_ACTOR_BDAY = "//time[@itemprop='birthDate']";
    private static final String GET_ACTOR_NAME = "//h1[@class='h1celebrite']";

    public static void main(String[] args) {

        WebClient client = createWebClient();

        setProxyConfig(client);

        try {
            for (int i = 26; i > 0; i--) {
                String adjustedAlphabetUrl = ALPHABET_URL.substring(0, 4) + (alpha++) + ALPHABET_URL.substring(5);

                HtmlPage mainPage = client.getPage(BASE_URL + adjustedAlphabetUrl);

                List<Actor> actors = parsePageCelebs(client, mainPage, GET_ACTORS_PAGE, GET_ACTOR_BDAY, GET_ACTOR_NAME);

                for (Actor a : actors) {
                    System.out.println(a);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void setProxyConfig(WebClient client) {
        final String LOCALHOST = "localhost";
        final int PROXY_PORT = 3128;
        ProxyConfig proxyConfig = new ProxyConfig(LOCALHOST, PROXY_PORT);
        client.getOptions().setProxyConfig(proxyConfig);
    }

    public static WebClient createWebClient() {
        WebClient client = new WebClient();
        client.getOptions().setJavaScriptEnabled(false);
        client.getOptions().setCssEnabled(false);
        client.getOptions().setUseInsecureSSL(true);
        return client;
    }

    static List<Actor> parsePageCelebs(WebClient client, HtmlPage mainPage, String GET_ACTORS_PAGE, String GET_ACTOR_BDAY, String GET_ACTOR_NAME) throws java.io.IOException {
        List<Actor> actors = new ArrayList<Actor>();

        List<HtmlAnchor> listHtmlPagesActors = (List<HtmlAnchor>) mainPage.getByXPath(GET_ACTORS_PAGE);
        for (int i = 0; i < listHtmlPagesActors.size(); i++) {
            HtmlAnchor actorPageAnchor = listHtmlPagesActors.get(i);
            String urlActorPage = actorPageAnchor.getHrefAttribute();

            HtmlPage actorHomePage = client.getPage(BASE_URL + urlActorPage);
            HtmlElement elementsBday = actorHomePage.getFirstByXPath(GET_ACTOR_BDAY);
            HtmlElement elementsName = actorHomePage.getFirstByXPath(GET_ACTOR_NAME);

            String bDayActor = elementsBday.asText();
            String nameActor = elementsName.asText();

            Actor actor = new Actor(nameActor, bDayActor);
            actors.add(actor);
        }
        return actors;
    }
}
